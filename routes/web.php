<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\StatistiqueController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'categorie'], function () {

        Route::post('/', 'App\Http\Controllers\CategorieController@store')->middleware(['permission:categories-create']);
        Route::get('/create', 'App\Http\Controllers\CategorieController@create')->middleware(['permission:categories-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\CategorieController@edit')->middleware(['permission:categories-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\CategorieController@destroy')->middleware(['permission:categories-delete']);
        Route::get('/{id}', 'App\Http\Controllers\CategorieController@show')->middleware(['permission:categories-read']);
        Route::patch('/{id}', 'App\Http\Controllers\CategorieController@update')->middleware(['permission:categories-update']);
        Route::get('/', 'App\Http\Controllers\CategorieController@index')->middleware(['permission:categories-read']);
    });
    Route::group(['prefix' => 'role'], function () {

        Route::post('/', 'App\Http\Controllers\RoleController@store')->middleware(['permission:users-create']);
        Route::get('/create', 'App\Http\Controllers\RoleController@create')->middleware(['permission:users-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\RoleController@edit')->middleware(['permission:users-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\RoleController@destroy')->middleware(['permission:users-delete']);
        Route::get('/{id}', 'App\Http\Controllers\RoleController@show')->middleware(['permission:users-read']);
        Route::patch('/{id}', 'App\Http\Controllers\RoleController@update')->middleware(['permission:users-update']);
        Route::get('/', 'App\Http\Controllers\RoleController@index')->middleware(['permission:users-read']);
    });

    Route::group(['prefix' => 'user'], function () {

        Route::post('/', 'App\Http\Controllers\UserController@store')->middleware(['permission:users-create']);
        Route::get('/create', 'App\Http\Controllers\UserController@create')->middleware(['permission:users-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\UserController@edit')->middleware(['permission:users-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\UserController@destroy')->middleware(['permission:users-delete']);
        Route::get('/{id}', 'App\Http\Controllers\UserController@show')->middleware(['permission:users-read']);
        Route::patch('/{id}', 'App\Http\Controllers\UserController@update')->middleware(['permission:users-update']);
        Route::get('/', 'App\Http\Controllers\UserController@index')->middleware(['permission:users-read']);
    });

    Route::group(['prefix' => 'fournisseur'], function () {


        Route::post('/', 'App\Http\Controllers\FournisseurController@store')->middleware(['permission:categories-create']);
        Route::get('/create', 'App\Http\Controllers\FournisseurController@create')->middleware(['permission:categories-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\FournisseurController@edit')->middleware(['permission:categories-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\FournisseurController@destroy')->middleware(['permission:categories-delete']);
        Route::get('/{id}', 'App\Http\Controllers\FournisseurController@show')->middleware(['permission:categories-read']);
        Route::patch('/{id}', 'App\Http\Controllers\FournisseurController@update')->middleware(['permission:categories-update']);
        Route::get('/', 'App\Http\Controllers\FournisseurController@index')->middleware(['permission:categories-read']);
    });

    Route::group(['prefix' => 'statut'], function () {

        Route::post('/', 'App\Http\Controllers\StatutController@store')->middleware(['permission:statuts-create']);
        Route::get('/create', 'App\Http\Controllers\StatutController@create')->middleware(['permission:statuts-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\StatutController@edit')->middleware(['permission:statuts-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\StatutController@destroy')->middleware(['permission:statuts-delete']);
        Route::get('/{id}', 'App\Http\Controllers\StatutController@show')->middleware(['permission:statuts-read']);
        Route::patch('/{id}', 'App\Http\Controllers\StatutController@update')->middleware(['permission:statuts-update']);
        Route::get('/', 'App\Http\Controllers\StatutController@index')->middleware(['permission:statuts-read']);
    });
    Route::group(['prefix' => 'materiel'], function () {

        Route::post('/', 'App\Http\Controllers\MaterielController@store')->middleware(['permission:materiels-create']);
        Route::get('/create', 'App\Http\Controllers\MaterielController@create')->middleware(['permission:materiels-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\MaterielController@edit')->middleware(['permission:materiels-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\MaterielController@destroy')->middleware(['permission:materiels-delete']);
        Route::get('/{id}', 'App\Http\Controllers\MaterielController@show')->middleware(['permission:materiels-read']);
        Route::patch('/{id}', 'App\Http\Controllers\MaterielController@update')->middleware(['permission:materiels-update']);
        Route::get('/', 'App\Http\Controllers\MaterielController@index')->middleware(['permission:materiels-read']);
        Route::patch('/assigner/{id}', 'App\Http\Controllers\MaterielController@assigner')->middleware(['permission:materiels-create']);

    });
    Route::get('/materiel_u', 'App\Http\Controllers\MaterielController@materiel_user')->middleware(['permission:materiels-read']);



    Route::group(['prefix' => 'demande'], function () {

        Route::post('/', 'App\Http\Controllers\DemandeController@store')->middleware(['permission:categories-create']);
        Route::get('/create', 'App\Http\Controllers\DemandeController@create')->middleware(['permission:categories-create']);
        Route::get('/{id}/edit', 'App\Http\Controllers\DemandeController@edit')->middleware(['permission:categories-update']);
        Route::delete('/delete/{id}', 'App\Http\Controllers\DemandeController@delete')->middleware(['permission:categories-delete']);
        Route::get('/{id}', 'App\Http\Controllers\DemandeController@show')->middleware(['permission:categories-read']);
        Route::patch('/{id}', 'App\Http\Controllers\DemandeController@update')->middleware(['permission:categories-update']);
        Route::get('/', 'App\Http\Controllers\DemandeController@index')->middleware(['permission:categories-read']);
        Route::patch('/valider/{id}', 'App\Http\Controllers\DemandeController@valider')->middleware(['permission:categories-update']);
        Route::patch('/refuser/{id}', 'App\Http\Controllers\DemandeController@refuser')->middleware(['permission:categories-update']);
    });
    Route::get('/demande_refuser', 'App\Http\Controllers\DemandeController@index_refuser');
    Route::get('/demande_valider', 'App\Http\Controllers\DemandeController@index_valider');

    Route::get('/demande_u', 'App\Http\Controllers\DemandeController@demande_user');
    Route::get('/demande_user_ref', 'App\Http\Controllers\DemandeController@demande_user_ref');
    Route::get('/demande_user_val', 'App\Http\Controllers\DemandeController@demande_user_val');
});