<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'superadmin' => [
            'users' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'statuts' => 'c,r,u,d',
            'fournisseurs' => 'c,r,u,d',
            'materiels' => 'c,r,u,d',
            'etats' => 'c,r,u,d',
            'demandes' =>'c,r,u,d'
        ],
        'administrateur' => [
            'categories' => 'c,r,u,d',
            'statuts' => 'c,r,u,d',
            'fournisseurs' => 'c,r,u,d',
            'materiels' => 'c,r,u,d',
            'etats' => 'c,r,u,d',
            'demandes' =>'c,r,u,d'
        ],
        'user' => [
            'materiels' => 'r',
            'demandes' =>'c,r,u,d'
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
