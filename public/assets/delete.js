$('.delete-confirm').click(function(event) {
    var form =  $(this).closest("form");
    var name = $(this).data("name");
    event.preventDefault();
    swal({
        title: `Etes-vous sûr que vous voulez supprimer ceci?`,
        text: "Si vous le supprimez, il disparaîtra pour toujours.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        form.submit();
      }
    });
});