<?php

namespace App\Http\Controllers;

use App\Models\Statut;
use App\Http\Requests\StoreStatutRequest;
use App\Http\Requests\UpdateStatutRequest;
use Illuminate\Http\Request;

class StatutController extends Controller
{
    public function index(){
     
    
        $statut= Statut::get()->all();
            return view('statut.index', compact('statut'));
           
    }


    public function create()
    {
       
            return view('Statut.create');
           
    }
    public function store(Request $request)
    {
       
                $this->validate($request, [
                    'libelle_statut' => 'required',
                    'description_statut' => 'required',


                ]);
                $requestData = $request->all();


                Statut::create($requestData);

                return redirect('statut')->with('message', 'statut ajoutee!');
           
    }
    public function show($id)
    {      
                $statut = Statut::where('id', '=', $id)->first();

             return view('statut.show', compact('statut'));
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
       

        $statut = Statut::findOrFail($id);
        return view('statut.edit', compact('statut'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
       
                $this->validate($request, [
                    'libelle_statut' => 'required',
                    'description_statut' => 'required',

                ]);
                $requestData = $request->all();

                $statut = Statut::findOrFail($id);
                $statut->update($requestData);

                return redirect('statut')->with('info', 'statut mise a jour!');
           
    }
    public function destroy($id)
    {
       
                $delete=Statut::destroy($id);
                return redirect('statut')->with('message','statut delete!');

           
    }


}
