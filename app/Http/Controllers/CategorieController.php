<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Http\Requests\StoreCategorieRequest;
use App\Http\Requests\UpdateCategorieRequest;
use App\Http\Requests;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function index(){


        $categorie= Categorie::get()->all();
      
        return view('categorie.index', compact('categorie'));

    }

    public function create()
    {   
        return view('categorie.create');
            
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
            'libelle_categorie' => 'required',
            'description_categorie' => 'required',


        ]);
        $requestData = $request->all();


        Categorie::create($requestData);
        session()->flash('message','Categorie ajoutee!');

        return redirect('categorie');
            
    }

    public function show($id)
    { 
        $categorie = Categorie::where('id', '=', $id)->first();

        return view('categorie.show', compact('categorie'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        
        $categorie = Categorie::findOrFail($id);
        return view('categorie.edit', compact('categorie'));
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'libelle_categorie' => 'required',
            'description_categorie' => 'required',

        ]);
        $requestData = $request->all();

        $categorie = Categorie::findOrFail($id);
        $categorie->update($requestData);
     return redirect('categorie')->with('info', 'Categorie updated!');
            
    }
    public function destroy($id)
    {
        $delete =  Categorie::destroy($id);

        return redirect('categorie')->with('message','suppression effectuer avec success!');
            
    }
}
