<?php

namespace App\Http\Controllers;

use App\Models\Fournisseur;
use App\Http\Requests\StoreFournisseurRequest;
use App\Http\Requests\UpdateFournisseurRequest;
use App\Http\Requests;
use Illuminate\Http\Request;

class FournisseurController extends Controller
{
    public function index(){


        $fournisseur= Fournisseur::get()->all();
        return view('fournisseur.index', compact('fournisseur'));

    }

    public function create()
    {   
        return view('fournisseur.create');
            
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
            'nom_fournisseur' => 'required',
            'localisation' => 'required',


        ]);
        $requestData = $request->all();


        Fournisseur::create($requestData);
        session()->flash('message','Fournisseur ajoutee!');

        return redirect('fournisseur');
            
    }

    public function show($id)
    { 
        $fournisseur = Fournisseur::where('id', '=', $id)->first();

        return view('fournisseur.show', compact('fournisseur'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        
        $fournisseur = Fournisseur::findOrFail($id);
        return view('fournisseur.edit', compact('fournisseur'));
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom_fournisseur' => 'required',
            'localisation' => 'required',

        ]);
        $requestData = $request->all();

        $fournisseur = Fournisseur::findOrFail($id);
        $fournisseur->update($requestData);
     return redirect('fournisseur')->with('info', 'Fournisseur updated!');
            
    }
    public function destroy($id)
    {
        $delete =  Fournisseur::destroy($id);

        return redirect('fournisseur')->with('message','suppression effectuer avec success!');
            
    }
}
