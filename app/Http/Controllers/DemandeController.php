<?php

namespace App\Http\Controllers;

use App\Mail\InfosMarkdownMail;
use App\Mail\InfoMail;
use App\Mail\test;
use App\Models\Demande;
use App\Models\User;
use App\Models\Etat;
use App\Models\Role;
use Demandes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DemandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        
        // if( $role->name==="super_admin" || $role->name==="admin" )
           

        $demande = Demande::select('demandes.id','demandes.libelle_dmde','demandes.description_dmde','etats.libelle_etat','users.nom')
        ->join('etats','etats.id','=','demandes.id_etat')
        ->join('users','users.id','=','demandes.id_user')
        ->where('etats.libelle_etat','=','En attente')
        ->get();
        return view('demande.index', compact('demande'));

         

    }


    // fonction qui affichent les demandes refusées et qui peuvent etre valider au besoin

    public function index_refuser(Request $request)
    { 

        //if( $role->name==="super_admin" || $role->name==="admin" )
          

        $demande = Demande::select('demandes.id','demandes.libelle_dmde','demandes.description_dmde','etats.libelle_etat','users.nom')
        ->join('etats','etats.id','=','demandes.id_etat')
        ->join('users','users.id','=','demandes.id_user')
        ->where('etats.libelle_etat','=','Non Valider')
        ->get();
        return view('demande.index', compact('demande'));
      
      

    }


    // fonction qui affichent les demandes validées
    public function index_valider(Request $request)
    {

        //if( $role->name==="super_admin" || $role->name==="admin" )
      
        $demande = Demande::select('demandes.id','demandes.libelle_dmde','demandes.description_dmde','etats.libelle_etat','users.nom')
        ->join('etats','etats.id','=','demandes.id_etat')
        ->join('users','users.id','=','demandes.id_user')
        ->where('etats.libelle_etat','=','Valider')
        ->get();
        return view('demande.index', compact('demande'));
       

    }
    /**fonction liees au dashboard du user */

    /**fonction qui affiche les demnades effectuées par l'utilisateur connecté et qui sont en attente de traitement */
    public function demande_user(Request $request)
    { 

        // if( is_array($role['name'])==="utilisateur")
        $user=Auth::user(); 

        $demande = Demande::select('demandes.id','demandes.libelle_dmde',
        'demandes.description_dmde','etats.libelle_etat','users.nom')
        ->join('etats','etats.id','=','demandes.id_etat')
        ->join('users','users.id','=','demandes.id_user')
        ->where('id_user','=',$user->id)
        ->where('etats.libelle_etat','=','En attente')

        ->get();
           
    }

    /**fonction qui affiche les demnades effectuées par l'utilisateur connecté et qui sont validées*/
    public function demande_user_val(Request $request)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;

            if( is_array($role['name'])==="utilisateur")
            {

                $demande = Demande::select('demandes.id','demandes.libelle_dmde',
                'demandes.description_dmde','etats.libelle_etat','users.nom')
                ->join('etats','etats.id','=','demandes.id_etat')
                ->join('users','users.id','=','demandes.id_user')
                ->where('id_user','=',$user->id)
                ->where('etats.libelle_etat','=','Valider')
                ->get();
                return view('demande.index_user', compact('demande'));
            }

            else{
                return view('errors.403');
            }
        }
    }
    /**fonction qui affiche les demnades effectuées par l'utilisateur connecté et qui ont été refusées */

    public function demande_user_ref(Request $request)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;

            if( is_array($role['name'])==="utilisateur")
            {

                $demande = Demande::select('demandes.id','demandes.libelle_dmde',
                'demandes.description_dmde','etats.libelle_etat','users.nom')
                ->join('etats','etats.id','=','demandes.id_etat')
                ->join('users','users.id','=','demandes.id_user')
                ->where('id_user','=',$user->id)
                ->where('etats.libelle_etat','=','Non Valider')
                ->get();
                return view('demande.index_user', compact('demande'));
            }

            else{
                return view('errors.403');
            }
        }
    }
    /**fin */

   /* public function sendEmail(){

        Mail::to('test@gmail.test')->send(new test());
        return view('emails.test');
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $etat = Etat::all();
        $user = User::all();
        return view('demande.create',compact('etat','user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;
            //
            
                $this->validate($request, [

                    'libelle_dmde' => 'required',
                    'description_dmde' => 'required',
                    'id_etat' => 'required|exists:etats,id',
                    'id_user' => 'required|exists:users,id',
                ]);
                $requestData = $request->all();
                Demande::create($requestData);

                /*envoit de l'email aux admins*/
                /*recuperation des admins et super admin*/

                /*$role1 = Role::Where('name','=','super_admin')->first() ;
                $role2 = Role::Where('name','=','admin')->first() ;
                $user_email = DB::table('users')
                ->join('roles','roles.id','=','users.id_role')
                ->join('batiments','batiments.id','=','users.id_batiment')
                ->select('users.id as user_id','users.nom as user_name','users.prenom as user_prenom',
                'users.poste as user_poste','users.id_role as userole_id','users.email as user_email',
                'roles.name as role_l','batiments.libelle_bat as batiment')
                ->Where('users.id_role','=',$role1->id)
                ->orWhere('users.id_role','=',$role2->id)
                ->get() ;

               foreach( $user_email as $u){
                    if($u->role_l=="super_admin" || is_array($role['name'])==="admin" ){
                      $receive=$u->user_email;
                      Mail::to( $receive)->send(new InfosMarkdownMail());
                    }
                }*/
                
                if(is_array($role['name'])==="superadmin")
                {
                    return redirect('demande')->with('message', 'demande added!');
                }
                elseif(is_array($role['name'])==="utilisateur")
                {
                    return redirect('demande_u')->with('message', 'demande added!');
                }
            }
            
    }



    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $demande = Demande::findOrFail($id);
        $etat = Etat::findOrFail($demande['id_etat']) ;
        $user = User::findOrFail($demande['id_user']) ;

        return view('demande.show', compact('demande','etat','user'));
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $demande = Demande::findOrFail($id);
        $etat = Etat::all();
        $user = User::all();
        return view('demande.edit',compact('demande','etat','user'));
           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    /*fonction qui permet de faire la mise a jour d' une demande */
    public function update(Request $request, $id)
    { $user=Auth::user();

        if($user)
        {


            $role = Role::Where('id','=',$user->id_role)->first() ;
            //
            if(is_array($role['name'])==="superadmin"  || is_array($role['name'])==="administrateur"|| is_array($role['name'])==="utilisateur" )
            {
                $this->validate($request, [


                    'libelle_dmde' => 'required',
                    'description_dmde' => 'required',
                    'id_etat' => 'required|exists:etats,id',

                    'id_user' => 'required|exists:users,id',

                ]);
                $requestData = $request->all();

                $demande = Demande::findOrFail($id);
                $demande->update($requestData);
                //si la modification est effectuee par l'admin ou le super admin, redirection vers la page demande de ceuli ci
                if( is_array( $role['name'])==="superadmin"  ||is_array( $role['name'])==="administrateur")
                {
                return redirect('demande')->with('info','demande mise a jour!');
                }
                //si la modification est effectuee par l'utilisateur, redirection vers la page demande utilisateur

                if( is_array( $role['name'])==="superadmin"  ||is_array( $role['name'])==="administrateur"|| is_array($role['name'])==="utilisateur" )
                {
                    return redirect('demande_u')->with('info','demande mise a jour!');;

                }
            }
            else{
                return view('errors.403');

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    /*fonction qui permet de  supprimer une demande */

   public function delete($id)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;

            $delete = Demande::destroy($id);

            if(  is_array($role['name'])==="superadmin" )
            {
                return redirect('demande')->with('message');
            }
            elseif(is_array($role['name'])==="utilisateur"){
                return redirect('demande_u')->with('message');

            }
        else{
            return view('errors.403');
        }
    }
    }

    /*fonction qui permet de  valider une demande */
    public function valider(Request $request, $id)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;
            if(is_array($role['name'])==="superadmin"|| is_array($role['name'])==="administrateur")
            {
               /* $demande = Demande::all();
                $demande =  $demande::findOrFail($id);
                  $demande->id_etat=2;
               $demande->update(['id','=>',$demande->id_etat ]);*/
               $etat=Etat::where('libelle_etat','=','Valider')->first();
               $demande =  Demande::where('id',$id)->first();
               $demande->update(['id_etat'=>$etat->id ]);
               /*recuperation de la personne qui a fait la demande et envoit de l'email de confirmation ou de refus */

                $result = DB::table('demandes')
                ->join('users','users.id','=','demandes.id_user')
                ->join('etats','etats.id','=','demandes.id_etat')
               ->select('demandes.id as demande.id','demandes.libelle_dmde as nom','demandes.description_dmde as description',
               'etats.nom_etat as etat','users.nom as name','users.email as email')
              ->where('demandes.id','=',$id) ->where('nom_etat','=','Valider')->first();
                $email=$result->email;
                Mail::to($email)->send(new InfoMail());
                return redirect('demande')->with('info', 'demande validee avec success!');}
            }
            else{
                return view('errors.403');
        }
    }

    /*fonction qui permet de  rejeter une demande */
    public function refuser(Request $request, $id)
    {
      
            $role = Role::first();
            if(is_array($role['name'])==="superadmin"|| is_array($role['name'])==="administrateur" )
            {
                /*$demande = Demande::all();
                $demande = Demande::findOrFail($id);
                $demande->id_etat=3;
                $demande->update(['id','=>',$demande->id_etat ]);*/
                $etat=Etat::where('nom_etat','=','Non Valider')->first();
                $demande =  Demande::where('id',$id)->first();
                $demande->update(['id_etat'=>$etat->id ]);
                return redirect('demande')->with('info', 'demande refusee!');
            }
            else{
                return view('errors.403');
            }
        
    }

}
