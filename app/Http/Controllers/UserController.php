<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
  use WithPagination;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    //fonction qui renvoit les utilsateurs creees
    public function index(Request $request)
    {
        
      $user=DB::table('role_user')   
      ->join('roles','roles.id','=','role_user.role_id')
      ->join('users','users.id','=','role_user.user_id')
    ->select('users.id','users.nom','users.prenom','users.fonction','users.email','roles.name')
     
      
      ->get();
      //dd($user);
      return view('user.index', compact('user'));
  
    }
  
      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      //fonction qui renvoit le formulaire de creations des  utilsateurs
      public function create()
      {  
        $role = Role::all();
        
        return view('user.create', compact('role'));
        
    }

              
  
      
  
  
  
      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       *
       */
  
      public function store(Request $request)
      {
         
  
        $this->validate($request, [

          'nom' => 'required',
          'prenom' => 'required',
          'fonction' => 'required',
          'email' => 'required',
          'password' => 'required|string|min:8',
          'role' => 'required',
          
        ]);
        $requestData = $request->all();
        $requestData['password'] = bcrypt($request->password);
    
        $user = User::create($requestData);
       
       
        $user->attachRole($request->input('role'));

         $user->save();

        /*$user=new User();
        $user->nom=$request->nom;
        $user->prenom=$request->prenom;
        $user->fonction=$request->fonction;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
     
      $r=$request['role'] ;
       $role = Role::where('name','=', $r)->first();
          
          $permission=Permission::select('permissions.*')
          ->join('permission_role','permission_role.permission_id','=','permissions.id')
          ->where('permission_role.role_id','=',$role->id)
          ->get();
        
          $role=Role::find($role->id)->first();
          $u=$user->attachRole($role);
         

        if(is_array($r)||is_object($r)){
          $role = Role::where('name','=', $r)->first();
         
          //$role=Role::find($role->id);
          $role1=Role::find($role->id);

          $user->attachRole($role1);
          foreach($permission as $item){
            $user->attachPermission($item);
  
         
         }
          /*foreach($request['role'] as $item){
            $user->attachRole($item);
          }
        }

      /*  $id_role=Role::where('name','=', $name_role)->first();

        $role = Role::all()->first();
        foreach (Permission::all() as $permission) {
          $role->givePermissionTo($permission);
        };

        $user->assignRole($role);

        $permission=Permission::all();
        $role=Role::all();
        foreach($permission as $item){
            $user2->attachPermission($item);

        }
        foreach($role->where('name','=','superadmin') as $item){
            $user2->attachRole($item);
            
        }*/



        //$user->save();
       //dd($user->role);
        /*$requestData = $request->all();
        $requestData['password'] = bcrypt( $requestData['password']);

        $user=User::create($requestData);
        //attribution du role et des permissions
        $name_role=$requestData['role'];
        $id_role=Role::where('name','=', $name_role)->first();
        $permission=Permission::select('permissions.*')
        ->join('permission_role','permission_role.permission_id','=','permissions.id')
        ->where('permission_role.role_id','=',$id_role->id)
        ->get();
        $role=Role::find($id_role->id);

        $user->attachRole($role);
        foreach($permission as $item){
          $user->attachPermission($item);

       }*/
        


      

        return redirect('user')->with('message', 'user added!');
    

  
      }
  
  
       //fonction qui renvoit les details sur un utilsateur
      /**
       * Display the specified resource.
       *
       * @param  \App\Models\user  $user
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      { 
        $user = User::findOrFail($id);
        

        return view('user.show', compact('user'));
        }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  \App\Models\user  $user
       * @return \Illuminate\Http\Response
       */
  
  
         //fonction qui renvoit le formulaire sur un utilsateur a modifier
      public function edit($id)
      { 
            $role = Role::all();
            $user = User::findOrFail($id);

            return view('user.edit', compact('user','role'));
        }
  
      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  \App\Models\user  $user
       * @return \Illuminate\Http\Response
       */
  
  
         //fonction qui valide les informations modifiees sur un utilsateur
      public function update(Request $request, $id)
      { 
        $this->validate($request, [

            'nom' => 'required',
            'prenom' => 'required',
            'fonction' => 'required',
            'email' => 'required',
            'password' => 'required|string|min:8',
            'role' => 'required',
        ]);

        $user=new User();
        $user->nom=$request->nom;
        $user->prenom=$request->prenom;
        $user->fonction=$request->fonction;
        $user->email=$request->email;
        $user->password=$request->password;
                  
      /* if($request['role']){
          foreach($request['role'] as $item){
            $user->attachRole($item);
          }
        }*/

        
      
        // $requestData = $request->all();

        
    
        $name_role=$request['role'];
        $id_role=Role::where('name','=', $name_role)->first();
        $user = user::findOrFail($id);
        
        $role = Role::find($id_role->$id);

        $permission=Permission::select('permissions.*')
        ->join('permission_role','permission_role.permission_id','=','permissions.id')
        ->where('permission_role.role_id','=',$id_role->id)
        ->get();
        
        $old_role=Role::select('roles.*')
        ->join('role_user','role_user.role_id','=','roles.id')
        ->where('role_user.user_id','=',$id)
        ->get();
        

        DB::table('permisssion_user')->where('user_id',$id)->delete();
      

      
        foreach($permission as $item){
          $user->attachPermission($item);

        }
        $user->attachRole($role);
        

        $user->update($request);

      
        return redirect('user')->with('info','user updated!');
      }
  
        //fonction qui supprime un utilsateur
      /**
       * Remove the specified resource from storage.
       *
       * @param  \App\Models\user  $user
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      { 
  
        $delete= User::destroy($id);
        return redirect('user')->with('message','user deleted!');
  
              
  
          
      }
  
  
  
  
  
  
  
  }
  
