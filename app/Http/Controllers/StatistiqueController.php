<?php

namespace App\Http\Controllers;

use App\Models\Materiel;
use App\Models\Categorie;

use App\Models\Role;
use App\Models\User;
use App\Models\Statut;
use App\Models\Fournisseur;
use App\Models\Demande;
use App\Models\Etat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatistiqueController extends Controller
{
    public function index()
    { 

        $fournisseur = Fournisseur::all()->count();
        $categorie = Categorie::all()->count();
        $materiel = Materiel::all()->count();
       //$user = User::all()->count();
        $statut = Statut::all()->count();
        $demande = Demande::all()->count();
        $utilisateur = User::all()->count();

        return view('home',compact('fournisseur','categorie','materiel','statut','demande','utilisateur'));
    }
}


