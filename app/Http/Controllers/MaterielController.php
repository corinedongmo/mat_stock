<?php

namespace App\Http\Controllers;

use App\Models\Materiel;
use App\Models\Categorie;

use App\Models\Role;
use App\Models\User;
use App\Models\Statut;
use App\Models\Fournisseur;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use vendor\laravel\framework\src\Illuminate\Support\helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use phpDocumentor\Reflection\Types\Nullable;


class MaterielController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $materiel = Materiel::select('materiels.id','materiels.reference','materiels.libelle_materiel','materiels.quantite','categories.libelle_categorie','Fournisseurs.nom_fournisseur','statuts.libelle_statut','users.nom'
        ,'materiels.id_user')
        ->join('categories','categories.id','=','materiels.id_categorie')
        ->join('Fournisseurs','Fournisseurs.id','=','materiels.id_Fournisseur')
        ->join('statuts','statuts.id','=','materiels.id_statut')
        ->join('users','users.id','=','materiels.id_user')
        ->join('users','users.id','=','materiels.id_user_auth')
        ->get();
        return view('materiel.index', compact('materiel'));
    }
        
    public function materiel_user(Request $request)
    { $user=Auth::user();

        
           $materiels = Materiel::select('materiels.id','materiels.reference','materiels.libelle_materiel','materiels.quantite',
           'categories.libelle_categorie','Fournisseurs.nom_fournisseur','statuts.libelle_statut',
           'users.nom'
           ,'materiels.id_user')
           ->join('categories','categories.id','=','materiels.id_categorie')
           ->join('Fournisseurs','Fournisseurs.id','=','materiels.id_Fournisseur')
           ->join('statuts','statuts.id','=','materiels.id_statut')
           //->join('users','users.id','=','materiels.id_user')
           ->join('users','users.id','=','materiels.id_user_auth')
           ->where('materiels.id_user','=',$user->id)
           //->where('users.nom','=',$user->name)
           ->get();
          // return ('materiels.id_user','=',$user->id);
           $use=User::select('users.nom','users.prenom')
           ->where('users.id','=',Auth::user()->id)
           ->first();


         // dd($use['prenom']);
           return view('materiel.index_user', compact('materiels','use'));
          
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\H ttp\Response
     */
    public function create()
    {
        
        // $image = Image::all();
        $categorie = Categorie::all();
        $fournisseur = Fournisseur::all();

        $statut = Statut::all();
        $user = User::all();
            return view('materiel.create', compact('statut','fournisseur','user','categorie'));
    
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
                $validator = Validator::make($request->all(), [
                    'nom' => 'required',
                    'reference' =>'required',
                    'quantite' => 'required',
                    'id_categorie' => 'required|exists:categories,id',
                    'id_Fournisseur' => 'required|exists:Fournisseurs,id',
                    'id_statut' => 'required|exists:statuts,id',
                    'id_user_auth'=> 'required|exists:users,id',
                    'id_user'=>'nullable',
                    'image_nom'=>'image|mimes:jpeg,jpg,png'
                ]);
                $requestData = $request->except('image');

                $requestData['reference']="T@s".rand();
                $mat= Materiel::create($requestData);

                $mat['reference']="T@s".$mat->id;
               // return $mat;
                $mat->update();

                if($request->has('image'))
                {
                    foreach($request->file('image') as $image)
                    {
                        $name=$image->getClientOriginalName();
                        $new_name='image_'.date("H-M-S").'-'.$name;
                        $image->storeAs('/materiel_images/', $new_name,'public');
                        $image=new Image();
                        $image->id_materiel=$mat->id;
                        $image->image_nom='storage/materiel_images/'. $new_name;
                        $image->save();

                    }
                }
                return redirect('materiel')->with('message', 'materiel ajoute!');

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */

     public function changement($obt,$nom_table, $nom_champ,$cle){
        $obt->$cle=DB::table($nom_table)
        ->select($nom_table.'.'.$nom_champ)
        ->where($nom_table.'.'.'id','=',$obt->$cle)->first();
        return $obt->cle="3";
        $obt->$cle=$obt->$cle->$nom_champ;

        /*$materiel->id_user=DB::table('users')
        ->select('users.nom')
        ->where('users.id','=',$materiel->id_user)->first();
        $materiel->id_user=$materiel->id_user->name;*/

     }
    public function show($id)
    {
        


                $materiel=Materiel::findOrfail($id);
                /*$this->changement($materiel,'categories','libelle_categorie',$materiel->id_categorie);
                return $materiel->id_categorie;
                $this->changement($materiel,'Fournisseurs','nom_fournisseur',$materiel->id_Fournisseur);
                //return $materiel->id_Fournisseur;

                $this->changement($materiel,'statuts','libelle_statut',$materiel->id_statut);
               // return $materiel->id_statut;

                $this->changement($materiel,'users','name',$materiel->id_user);
                return $materiel->id_user;
                */
                if($materiel->id_categorie){
                    $materiel->id_categorie=DB::table('categories')
                    ->select('categories.libelle_categorie')
                    ->where('categories.id','=',$materiel->id_categorie)->first();
                    $materiel->id_categorie=$materiel->id_categorie->libelle_categorie;
                }
                if($materiel->id_Fournisseur){
                    $materiel->id_Fournisseur=DB::table('Fournisseurs')
                    ->select('Fournisseurs.nom_fournisseur')
                    ->where('Fournisseurs.id','=',$materiel->id_Fournisseur)->first();
                    $materiel->id_Fournisseur=$materiel->id_Fournisseur->nom_fournisseur;
                }
                if($materiel->id_statut){
                    $materiel->id_statut=DB::table('statuts')
                    ->select('statuts.libelle_statut')
                    ->where('statuts.id','=',$materiel->id_statut)->first();
                    $materiel->id_statut=$materiel->id_statut->libelle_statut;
                }
                if($materiel->id_user){
                    $materiel->id_user=DB::table('users')
                    ->select('users.nom')
                    ->where('users.id','=',$materiel->id_user)->first();
                    $materiel->id_user=$materiel->id_user->name;
                }


               /* $materiel =DB ::table('materiels')
                ->join('categories','categories.id','=','materiels.id_categorie')
                 ->join('Fournisseurs','Fournisseurs.id','=','materiels.id_Fournisseur')
                ->join('statuts','statuts.id','=','materiels.id_statut')
                ->join('users','users.id','=','materiels.id_user')
                ->select('materiels.id','materiels.libelle_materiel','materiels.reference','materiels.quantite',
                'materiels.quantite',
                'users.nom as name_user','categories.libelle_categorie','Fournisseurs.nom_fournisseur','statuts.libelle_statut',
                )

                ->Where('materiels.id','=', $id)->first();
                  //dd($materiel);*/

                //$image = Image::findOrFail($materiel['id_image']) ;
                $img=  DB ::table('images')
                ->join('materiels','materiels.id','=','images.id_materiel')
                ->select('images.id as image_id','images.image_nom as image_nom','images.id_materiel as image_id_materiel')
                ->Where('images.id_materiel','=', $materiel->id)->get();

                /*$categorie = Categorie::findOrFail($materiel['id_categorie']);

                $Fournisseur = Fournisseur::findOrFail($materiel['id_Fournisseur']);

                $statut = Statut::findOrFail($materiel['id_statut']);*/

  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $materiel = Materiel::findOrFail($id);
                //dd($materiel);
                $image=  DB ::table('images')
                ->join('materiels','materiels.id','=','images.id_materiel')
                ->select('images.id as image_id','images.image_nom as image_nom','images.id_materiel as image_id_materiel')
                ->Where('images.id_materiel','=', $materiel->id)->get();


                $categorie = Categorie::all();
                $Fournisseur = Fournisseur::all();
                $statut = Statut::all();
                $user = User::all();
           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $this->validate($request, [

            'nom' => 'required',
            'reference' =>'required',
            'quantite' => 'required',

            'id_categorie' => 'required|exists:categories,id',
            'id_Fournisseur' => 'required|exists:Fournisseurs,id',
            'id_statut' => 'required|exists:statuts,id',
            'id_user_auth'=> 'required|exists:users,id',
            'id_user',
            'image_nom'=>'image|mimes:jpeg,jpg,png'
        ]);
        $requestData = $request->except('image');
        // return $requestData;

        $materiel = materiel::findOrFail($id);
        $materiel->update($requestData);



        if($request->has('image'))
        {
            $img=Image::where('images.id_materiel','=',$materiel->id)->delete();
            foreach($request->file('image') as $image)
            {

                $name=$image->getClientOriginalName();
                $new_name='image_'.date("H-M-S").'-'.$name;
                $image->storeAs('/materiel_images/', $new_name,'public');
                $image=new Image();
                $image->id_materiel=$materiel->id;
                $image->image_nom='storage/materiel_images/'. $new_name;

                $image->save();

            }
        }

                return redirect('materiel')->with('info', 'materiel mis a jour!');
            
            
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;

            if(  $role['libelle_role']==="super_admin" || $role['libelle_role']==="admin")
            {
                $delete =  Materiel::destroy($id);
                return redirect('materiel')->with('message','suppression effectuee avec success');

            }
            else{
                 return view('errors.403');
            }
        }

    }

    public function assigner(Request $request, $id)
    {
        $user=Auth::user();

        if($user)
        {
            $role = Role::Where('id','=',$user->id_role)->first() ;

            if(  $role['libelle_role']==="super_admin" || $role['libelle_role']==="admin")
            {

                //recuperer tous les personnels qui ont le role utilisateurs


                $materiel =  Materiel::where('id',$id)->first();
                $this->validate($request, [
                     'id_user' ,

                ]);
                $requestData = $request->id_user;
               // $mat= Materiel::create($requestData);
                $materiel->update(['id_user'=>$requestData]);

             return redirect('materiel')->with('message','materiel assigne avec success!');

            }
            else{
                 return view('errors.403');
            }
        }

    }
    

}