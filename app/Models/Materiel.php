<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materiel extends Model
{
    use HasFactory;
    protected $fillable = ['reference', 'libelle_materielle','quantite','image','id_user','id_categorie','id_statut','id_fournisseur'];

    protected $table='materiels';
    protected $primaryKey='id';

    public function user(){
        return $this->belongsTo('App\Models\User','id_user');
    
    }
    public function categorie(){
        return $this->belongsTo('App\Models\Categorie','id_categorie');
    
    }
    public function statut(){
        return $this->belongsTo('App\Models\statut','id_statut');
    
    }
    public function fournisseur(){
        return $this->belongsTo('App\Models\Fournisseur','id_fournisseur');
    
    }
 
}
