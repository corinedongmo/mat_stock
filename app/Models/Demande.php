<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    use HasFactory;
    protected $table='demandes';
    protected $primaryKey='id';


    protected $fillable=[ 'libelle_dmde','description_dmde', 'id_etat','id_user'];
    public function etat(){
        return $this->belongsTo('App\Models\Etat', 'id_etat');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','id_user');
    }
}
