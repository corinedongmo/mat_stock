<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $table='images';
    protected $primaryKey='id';


    protected $fillable=['image_nom', 'id_materiel'] ;
    public function setFilenamesAttribute($value)
    {
        $this->attributes['image_nom'] = json_encode($value);
    }
    public function id_materiel(){
        return $this->belongsTo('App\Models\Materiel');
    }
}
