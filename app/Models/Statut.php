<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statut extends Model
{
    use HasFactory;
    protected $guarded=[];

    protected $table='statuts';
    protected $primaryKey='id';


    public function materiels(){
        return $this->hasMany('App\Models\Materiel');
    }
    public function statut(){
        return $this->belongsTo('App\Models\Statut','id_statut');
    }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [

    ];
}
