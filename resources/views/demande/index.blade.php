@extends('layouts.apps')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{__('demande')}}</div>
                        <div class="card-body">
                             @if(Auth::user()->hasRole('superadmin','utilisateur')) 
                                <a href="{{ url('/demande/create') }}" class="mb-2 mr-2 btn-hover-shine btn btn-success btn-sm" title="Ajouter demande">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                                </a>

                           @endif
                        <br><br>


                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <table style="width: 100%;" id="example"
                                    class="table table-hover table-striped table-bordered">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{__('libelle demande')}}</th>
                                                <th>{{__('description')}}</th>
                                                <th>{{__('etat de la demande')}}</th>
                                                <th>{{__('personnel')}}</th>
                                                <th>{{__('Actions')}}</th>


                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($demande as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->libelle_dmde }} </td>
                                                <td>{{ $item->description_dmde }}</td>
                                                <td>{{ $item->libelle_etat }} </td>



                                                <td>{{ $item->nom }}</td>
                                                <td >
                                                    <a href="{{ url('/demande/' . $item->id) }}" title="Voir demande">
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                                        </button></a>

                                                    @if(Auth::user()->hasRole('superadmin','utilisateur')) 
                                                        <a href="{{ url('/demande/' . $item->id . '/edit') }}" title="Editer demande"><button class="mb-2 mr-2 btn-sm btn-hover-shine btn btn-primary
                                                            ">
                                                            <i class="fa fa-pen" aria-hidden="true"></i> </button>
                                                        </a>
                                                    @endif


                                                
                                                @if(Auth::user()->hasRole('superadmin')) 
                                                    <form method="post" action="{{ url('/demande/delete/' . $item->id) }}"style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        @csrf
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-danger btn-sm delete-confirm" title="supprimer demande"type="submit">
                                                            <i class="fa fa-trash"></i>
                                                           </button>
                                                    </form>
                                                @endif
                                               
                                                @if(Auth::user()->hasRole('superadmin','administrateur')) 
                                                    @if($item->libelle_etat=="Valider")
                                                        <button disabled class="valider mb-2 mr-2 btn btn-success disabled" title="Valider etat"id="valider">
                                                        <i class="fa fa-check" aria-hidden="true"></i>  valider</button>

                                                        
                                                    @elseif($item->libelle_etat=="Non Valider")
                                                        <!-- {{--<button disabled type="submit"id="refuser" class="refuser  mb-2 mr-2 btn btn-danger disabled" title="Refuser etat" >
                                                        <i class="fas fa-times" aria-hidden="true"></i> refuser</button>--}} -->

                                                        <form method="POST" action="{{ url('/demande/valider/'. $item->id)}}" accept-charset="UTF-8" style="display:inline">
                                                            {{ method_field('PATCH') }}
                                                            {{ csrf_field() }}
                                                                <button type="submit"
                                                                class="valider   mb-2 mr-2 border-0 btn-transition btn btn-outline-success" title="Valider etat"id="valider">
                                                                    <i class="fa fa-check" aria-hidden="true"></i>  valider</button>
                                                        </form>
                                                    @else
                                                        <form method="POST" action="{{ url('/demande/valider/'. $item->id)}}" accept-charset="UTF-8" style="display:inline">
                                                            {{ method_field('PATCH') }}
                                                            {{ csrf_field() }}
                                                                <button type="submit"
                                                                class="valider   mb-2 mr-2 border-0 btn-transition btn btn-outline-success" title="Valider etat"id="valider">
                                                                    <i class="fa fa-check" aria-hidden="true"></i>  valider</button>
                                                        </form>
                                                        <form method="POST"  action="{{ url('/demande/refuser/'. $item->id)}}" accept-charset="UTF-8" style="display:inline">
                                                            {{ method_field('PATCH') }}
                                                            {{ csrf_field() }}
                                                            <button type="submit"id="refuser" class="refuser mb-2 mr-2 border-0 btn-transition btn btn-outline-danger" title="Refuser etat" >
                                                                <i class="fas fa-times" aria-hidden="true"></i>  refuser</button>
                                                        </form>
                                                    @endif
                                               @endif

                                              
                                                </td>
                                               
                                             </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('assets/js/demande.js') }}"></script>


@endsection
