<div class="form-group {{ $errors->has('libelle_dmde') ? 'has-error' : ''}}">
    <label for="libelle_dmde" class="control-label">Nom du materiel <span style="color:red">*</span></label>
    <input class="form-control" name="libelle_dmde" type="text" id="libelle_dmde" value="{{ isset($demande->libelle_dmde) ? $demande->libelle_dmde : ''}}" required>

        {!! $errors->first('libelle_dmde', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group row {{ $errors->has('description_dmde') ? 'has-error' : ''}}">
    <label class="col-sm-12 col-md-2 col-form-label">description de la demande <span style="color:red">*</span></label>
    <div class="col-sm-12 col-md-12">
        <textarea class="form-control" rows="7" name="description_dmde" type="textarea" id="description_dmde" required>{{ isset($demande->description_dmde) ? $demande->description_dmde : ''}}</textarea>
        {!! $errors->first('description_dmde', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('id_etat') ? 'has-error' : ''}}" >
    <input class="form-control" name="id_etat" type="hidden" id="id_etat" value="{{ isset($etat->id_etat) ? $etat->id_etat : '1'}}"required >


</div>


<div class="form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
    <input class="form-control" name="id_user" type="hidden" id="id_user"
    value=" {{ isset($demande->id_user) ? $demande->id_user : Auth::user()->id }}"required >

</div>














<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? __('Modifier') : __('Creer' )}}">
</div>
