@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Demande</div>
                    <div class="card-body">
                        <div style="float:right">
                            @if(Auth::user()->hasRole('utilisateur')) 
                                <a href="{{ url('/demande_u/') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            @endif

                            <a href="{{ url('/demande/' . $demande->id . '/edit') }}" title="Editer demande"><button class="btn btn-primary btn-sm"><i class="fa fa-pen" aria-hidden="true"></i> Modifier</button></a>
                        </div>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <th>Libelle demande </th>
                                        <td> {{ $demande->libelle_dmde }} </td>
                                    </tr>


                                        <th>Description</th>


                                        <td> {{ $demande->description_dmde }} </td>
                                    </tr>
                                    <tr>
                                        <th> Etat de la demande </th>
                                        <td> {{ $etat->libelle_etat }} </td>
                                    </tr>
                                    <tr>

                                        <th>{{__('Personnel')}}  </th>
                                        <td> {{ $user->nom }} &nbsp; {{ $user->prenom }} </td>
                                        <th>    Nom Personnel   </th>
                                        <td> {{ $user->nom }} </td>

                                    </tr>

                                    <tr>
                                        <th>Prenom Personnel  </th>
                                        <td> {{ $user->prenom }} </td>
                                    </tr>





                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
