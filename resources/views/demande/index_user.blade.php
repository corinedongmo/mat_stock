@extends('layouts.apps')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">demande</div>
                        <div class="card-body">
                           
                            @if(Auth::user()->hasRole('superadmin','utilisateur')) 
                                <a href="{{ url('/demande/create') }}" class="mb-2 mr-2 btn-hover-shine btn btn-success btn-sm" title="Ajouter demande">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                                </a>

                           @endif
                            <br><br>


                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <table style="width: 100%;" id="example"
                                    class="table table-hover table-striped table-bordered">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{__('libelle demande')}}</th>
                                                <th>{{__('description')}}</th>
                                                <th>{{__('etat de la demande')}}</th>
                                                <th>{{__('personnel')}}</th>
                                                <th>{{__('Actions')}}</th>


                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($demande as $item)
                                            <tr>

                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->libelle_dmde }}
                                                <td>{{ $item->description_dmde }}</td>
                                                <td>
                                                @if($item->nom_etat=="Valider")
                                                &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp 
                                                    <button disabled class="valider mb-2 mr-2 btn btn-success " title="Valider etat"id="valider">
                                                    <i class="fa fa-check" aria-hidden="true"></i> valider</button>


                                                @elseif($item->nom_etat=="Non Valider")
                                                &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp 
                                                    <button disabled type="submit"id="refuser" class="refuser  mb-2 mr-2 btn btn-danger" title="Refuser etat" >
                                                    <i class="fas fa-times" aria-hidden="true"></i> refuser</button>

                                                   
                                                @else
                                                &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp 
                                                <button align ="center"disabled type="submit"id="en attente" class="en attente  mb-2 mr-2 btn-hover-shine btn btn-info " title="en attente etat" >
                                                    En attente
                                                </button>
                                                    
                                                @endif
                                               
                                                </td>
                                                <td>{{ $item->name }}</td>
                                                <td >
                                                    <a href="{{ url('/demande/' . $item->id) }}" title="Voir demande">
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                                        </button>
                                                    </a>
                                                    <a href="{{ url('/demande/' . $item->id . '/edit') }}" title="Editer demande">
                                                        <button class="mb-2 mr-2 btn-hover-shine btn-sm btn btn-primary ">
                                                            <i class="fa fa-pen" aria-hidden="true"></i>
                                                        </button>
                                                    </a>

                                                    <form method="post" action="{{ url('/demande/delete/' . $item->id) }}"style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        @csrf
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-danger btn-sm delete-confirm" title="supprimer demande"type="submit">
                                                            <i class="fa fa-trash"></i>
                                                           </button>
                                                    </form>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
