
@extends('layouts.apps')

@section('content')
    @if(Auth::user()->hasRole('utilisateur'))
        <div class="row justify-content-around" >

            <div class="col-lg-5 col-xs-6 ">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$demande}}</h3>
                        <p>Demandes</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href=" {{url('demande')}} " class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-5 col-xs-6 align-self-center">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$materiel}}</h3>
                        <p>Materiels</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="{{url('materiel')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
    
        </div>
    @endif

      @if(Auth::user()->hasRole('superadmin','administrateur'))
        <div class="row justify-content-around" >

            <div class="col-lg-5 col-xs-6 ">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$demande}}</h3>
                        <p>Demandes</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href=" {{url('demande_u')}} " class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-5 col-xs-6 align-self-center">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$materiel}}</h3>
                        <p>Materiels</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="{{url('materiel_u')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
    
        </div>
   
        <div class="row justify-content-around">

            <div class="col-lg-5 col-xs-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$categorie}}</h3>
                        <p>Categories</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="{{url('categorie')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-5 col-xs-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$fournisseur}}</h3>
                        <p>Fournisseurs</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="{{url('fournisseur')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
    
        </div>
       
        <div class="row justify-content-around">

        <div class="col-lg-5 col-xs-6">

            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$statut}}</h3>
                    <p>Statuts</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{url('statut')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div> 
        </div>
    @endif
    @if(Auth::user()->hasRole('superadmin','administrateur'))
        <div class="col-lg-5 col-xs-6">

            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$utilisateur}}</h3>
                    <p>Utilisateurs</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{url('user')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
   
    </div>
    @endif
@endsection
