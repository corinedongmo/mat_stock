@extends('layouts.apps')

@section('content')

    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Statut</div>
                    <div class="card-body">
                        <a href="{{ url('/statut/create') }}" class="mb-2 mr-2 btn-hover-shine btn btn-success btn-sm" title="Ajouter statut">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                        </a>
                        &nbsp;&nbsp;

                       <br><br>


                        <div class="main-card mb-3 card">
                            <div class="card-body">


                                <table style="width: 100%;" id="example1"
                                    class="table table-hover table-striped table-bordered">

                                    <thead>
                                        <tr>


                                            <th>#</th>
                                            <th>{{__('Libelle Du statut')}}</th>
                                            <th>{{__('Description ')}}</th>

                                            <th>{{__('Actions')}}</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($statut as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>{{ $item->libelle_statut }}</td>
                                            <td>{{ $item->description_statut }}</td>


                                            <td >
                                                <a href="{{ url('/statut/' . $item->id) }}" title="Voir statut"><button class="mb-2 mr-2 btn-hover-shine btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                                <a href="{{ url('/statut/' . $item->id . '/edit') }}" title="Editer statut"><button class="mb-2 mr-2 btn-hover-shine btn btn-primary btn-sm"><i class="fa fa-pen" aria-hidden="true"></i></button></a>

                                                <form method="post" action="{{ url('/statut/delete/' . $item->id) }}"style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    @csrf
                                                    <button class="mb-2 mr-2 btn-hover-shine btn btn-danger btn-sm delete-confirm" title ="supprimer statut" type="submit">
                                                        <i class="fa fa-trash"></i>
                                                       </button>
                                                </form>


                                                 
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
