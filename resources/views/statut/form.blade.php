<div class="form-group row {{ $errors->has('libelle_statut') ? 'has-error' : ''}}">
            <label class="col-sm-12 col-md-2 col-form-label">Nom Du statut</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text"  name="libelle_statut"required value="{{isset($statut->libelle_statut) ? $statut->libelle_statut : ''}}">
            </div>
        </div>
        <div class="form-group row {{ $errors->has('description_statut') ? 'has-error' : ''}}">
            <label class="col-sm-12 col-md-2 col-form-label">description du statut</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" rows="5" name="description_statut" type="textarea" id="description_statut" required>{{ isset($statut->description_statut) ? $statut->description_statut : ''}}</textarea>
                {!! $errors->first('description_statut', '<p class="help-block">:message</p>') !!}
            </div>
        </div>





        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Creer'  }}">
        </div>

