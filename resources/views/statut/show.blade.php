@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Statut </div>
                    <div class="card-body">
                        <div style="float:right">
                        <a href="{{ url('/statut') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>

                        <a href="{{ url('/statut/' . $statut->id . '/edit') }}" title="Edit statut"><button class="btn btn-primary btn-sm"><i class="fa fa-pen" aria-hidden="true"></i> Modifier</button></a>
                    </div>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <th> Libelle statut </th>
                                        <td> {{ $statut->libelle_statut }} </td>
                                    </tr>
                                    <tr>
                                        <th>Description </th>
                                        <td> {{ $statut->description_statut }} </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
