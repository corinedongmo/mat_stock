@extends('layouts.apps')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Ajouter un statut</div>
                <div class="card-body">
                <div style="float:right">
                 <a href="{{ url('/statut') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                    </div>
                    <br />
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/statut') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}




                        @include ('statut.form', ['formMode' => 'create'])


                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

