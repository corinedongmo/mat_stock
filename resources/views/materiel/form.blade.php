

<div class="form-group {{ $errors->has('libelle_materiel') ? 'has-error' : ''}}">
    <label for="libelle_materiel" class="control-label">Nom Du Materiel<span style="color:red">*</span></label>
    <input class="form-control" name="libelle_materiel" type="text" id="libelle_materiel" value="{{ isset($materiel->libelle_materiel) ? $materiel->libelle_materiel : ''}}" required>
    {!! $errors->first('libelle_materiel', '<p class="help-block">:message</p>') !!}
</div>
{{--<div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    <label for="reference" class="control-label">Reference<span style="color:red">*</span></label>
    <input class="form-control" name="reference" type="text" id="reference" value="{{ isset($materiel->reference) ? $materiel->reference : ''}}" required>

        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
</div>--}}

<div class="form-group row {{ $errors->has('quantite') ? 'has-error' : ''}}">
    <label class="description">Quantite <span style="color:red">*</span></label>
 <div class="col-sm-12 col-md-12">
 
        <input type="number" class="form-control" rows="7" name="quantite" type="textarea" id="quantite" required>{{ isset($materiel->quantite) ? $materiel->quantite : ''}}
        {!! $errors->first('quantite', '<p class="help-block">:message</p>') !!}
</div>
</div>


<div class="form-group {{ $errors->has('id_categorie') ? 'has-error' : ''}}">
    <label for="id_categorie" class="control-label">Categorie <span style="color:red">*</span></label>


    <select class="form-control" name="id_categorie" type="number" id="id_categorie" value="{{ isset($materiel->id_categorie) ? $materiel->id_categorie : '' }}" required>
        <option value="">{{ __('Selectionner le categorie ') }} </option>
        @foreach($categorie as $categorie)
        <option value="{{$categorie->id}}" @if(isset($materiel) && $categorie->id == $materiel->id_categorie) selected @endif>{{$categorie->libelle_categorie}}</option>
        @endforeach
    </select>
    {!! $errors->first('id_categorie', '<p class="help-block">:message</p>') !!}

</div>

<div class="form-group {{ $errors->has('id_fournisseur') ? 'has-error' : ''}}">
    <label for="id_fournisseur" class="control-label">fournisseur <span style="color:red">*</span></label>


    <select class="form-control" name="id_fournisseur" type="number" id="id_fournisseur" value="{{ isset($materiel->id_fournisseur) ? $materiel->id_fournisseur : '' }}" required>
        <option value="">{{ __('Selectionner le fournisseur ') }} </option>
        @foreach($fournisseur as $fournisseur)
        <option value="{{$fournisseur->id}}" @if(isset($materiel) && $fournisseur->id == $materiel->id_fournisseur) selected @endif>{{$fournisseur->nom_fournisseur}}</option>
        @endforeach
    </select>
    {!! $errors->first('id_fournisseur', '<p class="help-block">:message</p>') !!}

</div>


<div class="form-group {{ $errors->has('id_statut') ? 'has-error' : ''}}">
    <label for="id_statut" class="control-label">Statut <span style="color:red">*</span></label>


    <select class="form-control" name="id_statut" type="number" id="id_statut" value="{{ isset($materiel->id_statut) ? $materiel->id_statut : '' }}" required>
        <option value="">{{ __('Selectionner le statut ') }} </option>
        @foreach($statut as $statut)
        <option value="{{$statut->id}}" @if(isset($materiel) && $statut->id == $materiel->id_statut) selected @endif>{{$statut->libelle_statut}}</option>
        @endforeach
    </select>
    {!! $errors->first('id_statut', '<p class="help-block">:message</p>') !!}

</div>



<div class="form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
    <label for="id_user" class="control-label">user<span style="color:red">*</span></label>


    <select class="form-control" name="id_user" type="number" id="id_user" value="{{ isset($materiel->id_user) ? $materiel->id_user : '' }}" >
        <option value="">{{ __('Selectionner le user ') }} </option>
        @foreach($user as $user)
        <option value="{{$user->id}}" @if(isset($materiel) && $user->id == $materiel->id_user) selected @endif>{{$user->nom}}</option>
        @endforeach
    </select>
    {!! $errors->first('id_user', '<p class="help-block">:message</p>') !!}

</div>
 <div class="form-group {{ $errors->has('id_user_auth') ? 'has-error' : ''}}">
    <input class="form-control" name="id_user_auth" type="hidden" id="id_user_auth"
    value=" {{ isset($materiel->id_user_auth) ? $materiel->id_user_auth : Auth::user()->id }}"required >

</div> 



<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image_nom" class="control-label">image<span style="color:red">*</span></label>
    <input type="file" name="image[]" class="form-control" multiple="multiple">




    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}


</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? __('Modifier') : __('Creer' )}}">
</div>
