@extends('layouts.apps')

@section('content')
<style>
    .conteneur{
        display: inline-flex;
    }
    </style>
    
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Materiel</div>
                    <div class="card-body">



                        <div class="main-card mb-3 card">
                            <div class="card-body">

                                @if(Auth::user()->hasRole('superadmin','utilisateur')) 
                                   <table style="width: 100%;" id="example"
                                    class="table table-hover table-striped table-bordered">

                                    <thead>
                                        <tr>


                                            <th>#</th>
                                            <th>nom</th>

                                            <th>Reference</th>

                                            <th>categorie</th>
                                            <th>batiment</th>
                                            <th>statut</th>
                                            <th>personnel</th>
                                            <th>Actions</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($materiels as $item )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>{{ $item->nom }}</td>
                                            <td>{{ $item->reference }}</td>
                                           
                                            <td>{{ $item->libelle_cat }}</td>
                                            <td>{{ $item->libelle_bat }}</td>

                                            <td class=".conteneur">
                                                <button class=" btn btn-alternate active"><style>

                                                </style>{{ $item->libelle_statut }}</button>
                                            </td>
                                            <td>{{$use->nom}} {{$use->prenom}}</td>
                                            
                                            <td>
                                                <a href="{{ url('/materiel/' . $item->id) }}" title="Voir materiel">
                                                    <button class="mb-2 mr-2 btn-hover-shine btn-sm btn btn-info "><i class="fa fa-eye" aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                            </td>
                                       </tr>
                                    @endforeach
                              
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                </div>
                </div>
            </div>
        </div>
    </div>
  
@endsection
