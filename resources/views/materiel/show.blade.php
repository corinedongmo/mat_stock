@extends('layouts.apps')

@section('content')


<div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel" data-slide-to="0" class=""></li>
      <?php
          for ($i=1; $i < count($img); $i++) {
      ?>
          <li data-target="#carousel" data-slide-to="'.$i.'" class=""></li>

      <?php } ?>

    </ol>
    <div class="carousel-inner" role="listbox">

      @foreach ($img as $i)
          <div class="carousel-item">
              <img class="d-block" src="{{URL::to($i->image_nom)}}" alt="First slide">
              <div class="carousel-caption d-none d-md-block">
                  <h5>Nature, United States</h5>
              </div>
          </div>
      @endforeach


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{__('materiel')}}</div>
                    <div class="card-body">
                        <div style="float:right">
                            <a href="{{ url('/materiel/') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            
                            @if(Auth::user()->hasRole('utilisateur')) 
                                <a href="{{ url('/materiel_u/') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            @endif
                            @if(Auth::user()->hasRole('superadmin','administrateur')) 
                                <a href="{{ url('/materiel/' . $materiel->id . '/edit') }}" title="Editer materiel">
                                    <button class="btn btn-primary btn-sm"><i class="fa fa-pen" aria-hidden="true"></i>
                                        Modifier</button></a>
                            @endif
                        </div>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <th> {{__('Nom')}} </th>
                                        <td> {{ $materiel->libelle_materiel }} </td>
                                    </tr>
                                    <tr>
                                        <th> {{__('Reference')}} </th>
                                        <td> {{ $materiel->reference }} </td>
                                    </tr>
                                    <tr>
                                        <th>{{__('Quantité')}} </th>
                                        <td> {{ $materiel->quantite }} </td>
                                    </tr>
                                    <tr>
                                        <th> {{__('Statut')}} </th>
                                        <td> {{ $materiel->id_statut }} </td>
                                    </tr>
                                    <tr>
                                        <th> {{__('Fournisseur')}} </th>
                                        <td> {{ $materiel->id_fournisseur }} </td>
                                    </tr>
                                    <tr>
                                        <th>{{__('Personnel')}}  </th>
                                        <td> {{ $materiel->id_user }} </td>
                                    </tr>
                                    <tr>
                                        <th> {{__('Categorie')}} </th>
                                        <td> {{ $materiel->id_categorie }} </td>
                                    </tr>
                                    <tr>
                                        <th> Image </th>
                                        <td class="md-10">
                                            <div id="carousel" class="carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators">
                                                  <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                                  <?php
                                                      for ($i=1; $i < count($img); $i++) {
                                                  ?>
                                                      <li data-target="#carousel" data-slide-to="{{$i}}" class=""></li>

                                                  <?php } ?>

                                                </ol>
                                                <div class="carousel-inner"  role="listbox">

                                                    @for ($i=0;$i<count($img);$i++)
                                                        @if ($i==0)
                                                            <div class="carousel-item active">
                                                                <img class="" src="{{URL::to($img[$i]->image_nom)}}" alt="First slide"  style="width:30%; heigth:20%">

                                                            </div>
                                                        @else
                                                            <div class="carousel-item">
                                                                <img class="" src="{{URL::to($img[$i]->image_nom)}}" alt="First slide"  style="width:30%; heigth:20%">

                                                            </div>
                                                        @endif

                                                    @endfor

                                                </div>
                                                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                                  <i class="carousel-control-prev-icon"></i>
                                                </a>
                                                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                                  <i class="carousel-control-next-icon"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

@endsection
