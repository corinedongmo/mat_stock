@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Modifier materiel</div>
                    <div class="card-body">
                        <div style="float:right">
                        <a href="{{ url('/materiel') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{__('Retour')}}</button></a>
                        </div>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/materiel/' . $materiel->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}


                            <div class="form-group {{ $errors->has('libelle_materiel') ? 'has-error' : ''}}">
                                <label for="libelle_materiel" class="control-label">libelle_materiel Du Materiel<span style="color:red">*</span></label>
                                <input class="form-control" name="libelle_materiel" type="text" id="libelle_materiel" value="{{ isset($materiel->libelle_materiel) ? $materiel->libelle_materiel : ''}}" required>
                                {!! $errors->first('libelle_materiel', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
                                <label for="reference" class="control-label">Reference<span style="color:red">*</span></label>
                                <input class="form-control" name="reference" type="text" id="reference" value="{{ isset($materiel->reference) ? $materiel->reference : ''}}" required>

                                    {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {{ $errors->has('description_m') ? 'has-error' : ''}}">
                                <label for="description_m" class="control-label">description<span style="color:red">*</span></label>
                                <textarea class="form-control" name="description_m" type="text" id="description_m"  required>{{ isset($materiel->description_m) ? $materiel->description_m : ''}}
                                </textarea>
                                    {!! $errors->first('description_m', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('id_categorie') ? 'has-error' : ''}}">
                                <label for="id_categorie" class="control-label">Categorie <span style="color:red">*</span></label>


                                <select class="form-control" name="id_categorie" type="number" id="id_categorie" value="{{ isset($materiel->id_categorie) ? $materiel->id_categorie : '' }}" required>
                                    <option value="">{{ __('Selectionner le categorie ') }} </option>
                                    @foreach($categorie as $categorie)
                                    <option value="{{$categorie->id}}" @if(isset($materiel) && $categorie->id == $materiel->id_categorie) selected @endif>{{$categorie->libelle_cat}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('id_categorie', '<p class="help-block">:message</p>') !!}

                            </div>

                            <div class="form-group {{ $errors->has('id_fournisseur') ? 'has-error' : ''}}">
                                <label for="id_fournisseur" class="control-label">{{ __('fournisseur ') }}<span style="color:red">*</span></label>


                                <select class="form-control" name="id_fournisseur" type="number" id="id_fournisseur" value="{{ isset($materiel->id_batiment) ? $materiel->id_batiment : '' }}" required>
                                    <option value="">{{ __('Selectionner le fournisseur ') }} </option>
                                    @foreach($fournisseur as $fournisseur)
                                    <option value="{{$fournisseur->id}}" @if(isset($materiel) && $fournisseur->id == $materiel->id_fournisseur) selected @endif>{{$fournisseur->libelle_bat}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('id_fournisseur', '<p class="help-block">:message</p>') !!}

                            </div>


                            <div class="form-group {{ $errors->has('id_statut') ? 'has-error' : ''}}">
                                <label for="id_statut" class="control-label">{{ __('Statut ') }}<span style="color:red">*</span></label>


                                <select class="form-control" name="id_statut" type="number" id="id_statut" value="{{ isset($materiel->id_statut) ? $materiel->id_statut : '' }}" required>
                                    <option value="">{{ __('Selectionner le statut ') }} </option>
                                    @foreach($statut as $statut)
                                    <option value="{{$statut->id}}" @if(isset($materiel) && $statut->id == $materiel->id_statut) selected @endif>{{$statut->libelle_statut}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('id_statut', '<p class="help-block">:message</p>') !!}

                            </div>



                            <div class="form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
                                <label for="id_user" class="control-label">{{ __('user ') }}<span style="color:red">*</span></label>


                                <select class="form-control" name="id_user" type="number" id="id_user" value="{{ isset($materiel->id_user) ? $materiel->id_user : '' }}" >
                                    <option value="">{{ __('Selectionner le user ') }} </option>
                                    @foreach($user as $user)
                                    <option value="{{$user->id}}" @if(isset($materiel) && $user->id == $materiel->id_user) selected @endif>{{$user->nom}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('id_user', '<p class="help-block">:message</p>') !!}

                            </div>
                            <div class="form-group {{ $errors->has('id_user_auth') ? 'has-error' : ''}}">
                                <input class="form-control" name="id_user_auth" type="hidden" id="id_user_auth"
                                value=" {{ isset($materiel->id_user_auth) ? $materiel->id_user_auth : Auth::user()->id }}"required >

                            </div>



                            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                                <label for="image" class="control-label">image<span style="color:red">*</span></label>
                                <input type="file" name="image[]" class="form-control" multiple="multiple" value="{{ isset($i->image_nom) ? $i->image_nom : ''}}"
                                >

                                @foreach ($image as $i)
                                 @endforeach

                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                              <br>

<div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel" data-slide-to="0" class="active"></li>
      <?php
          for ($i=1; $i < count($image); $i++) {
      ?>
          <li data-target="#carousel" data-slide-to="{{$i}}" class=""></li>

      <?php } ?>

    </ol>
    <div class="carousel-inner" role="listbox">

        <div class="carousel-item active">
            <div class="carousel-caption d-none d-md-block">
                <h5>images</h5>
            </div>
        </div>
      @foreach ($image as $i)
          <div class="carousel-item">
              <img class="" src="{{URL::to($i->image_nom)}}" alt="First slide">
              <div class="carousel-caption d-none d-md-block">
                  <h5>Nature, United States</h5>
              </div>
          </div>
      @endforeach

    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
      <i class="carousel-control-prev-icon"></i>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
      <i class="carousel-control-next-icon"></i>
    </a>
</div>

                            </div>


                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value=" Modifier ">
                            </div>



                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
