@extends('layouts.apps')

@section('content')
<style>
    .conteneur{
        display: inline-flex;
    }
    </style>

    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Materiel</div>
                    <div class="card-body">
                        <?php
                            use App\Models\Role;
                            use App\Models\User;
                            use App\Models\Materiel;
                            $user=Auth::user();
                        ?>
                        

                        @if($user->hasRole('superadmin','administrateur')) 
                            <a href="{{ url('/materiel/create') }}" class="mb-2 mr-2 btn-hover-shine btn btn-success btn-sm" title="Ajouter materiel">
                                <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                            </a>
                       @endif

                       

                       <br><br>


                        <div class="main-card mb-3 card">
                            <div class="card-body">


                                <table style="width: 100%;" id="example"
                                    class="table table-hover table-striped table-bordered">

                                    <thead>
                                        <tr>


                                            <th>#</th>
                                            <th>{{__('nom')}}</th>

                                            <th>{{__('Reference')}}</th>

                                            <th>{{__('categorie')}}</th>
                                            <th>{{__('fournisseur')}}</th>
                                            <th>{{__('statut')}}</th>
                                            <th>{{__('personnel')}}</th>
                                            <th>{{__('Actions')}}</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($materiel as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>{{ $item->libelle_materiel }}</td>
                                            <td>{{ $item->reference }}</td>

                                            <td>{{ $item->libelle_categorie }}</td>
                                            <td>{{ $item->nom_fournisseur }}</td>

                                            <td class=".conteneur">
                                                <button class=" btn btn-alternate active"><style>

                                                </style>{{ $item->libelle_statut }}</button>
                                            </td>
                                            <td>{{ $item->id_user }}</td>

                                            <td>
                                                <a href="{{url('/materiel/' .$item->id)}}" title="Voir materiel">
                                                    <button class="mb-2 mr-2 btn-hover-shine btn-sm  btn btn-info "><i class="fa fa-eye" aria-hidden="true"></i>
                                                    </button>
                                                </a>

                                               @if(Auth::user()->hasRole('superadmin','administrateur')) 
                                                    <a href="{{ url('/materiel/' . $item->id . '/edit') }}" title="Editer materiel">
                                                        <button class="mb-2 mr-2 btn-hover-shine  btn btn-primary btn-sm"><i class="fa fa-pen" aria-hidden="true"></i>
                                                        </button>
                                                    </a>

                                                    <form method="post" action="{{ url('/materiel/delete/' . $item->id) }}"style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        @csrf
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-danger btn-sm delete-confirm" title="supprimer materiel" type="submit">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </form>

                                                    <div class="d-inline-block dropdown">
                                                        <button  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                                                <i class="fa fa-business-time fa-w-20"></i>
                                                            </span>
                                                           Assigner
                                                        </button>
                                                        {{
                                                           $role1 = Role::Where('name','=','utilisateur')->first() ;
                                                            $user=User::all()->Where('id_role','=',$role1->id);
                                                        }}
                                                            <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">

                                                                <div class="dropdown-menu-header">
                                                                    <div class="dropdown-menu-header-inner bg-info">
                                                                        <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');">

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            <form action="{{ url('materiel/assigner/'. $item->id) }}" accept-charset="UTF-8" method="POST">
                                                                {{ method_field('PATCH') }}
                                                                {{ csrf_field() }}

                                                                <div class="scroll-area-xs" style="height: 150px;">
                                                                    <div class="scrollbar-container ps">
                                                                        <ul class="nav flex-column">

                                                                            <li class="nav-item form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
                                                                                <label for="id_user" class="control-label">Utilisateur<span style="color:red">*</span></label>
                                                                                <select class="form-control" name="id_user" type="number" id="id_user" value="{{ isset($item->id_user) ? $item->id_user : '' }}" required>
                                                                                    <option value="">{{ __('Selectionner le user ') }} </option>
                                                                                    @foreach($user as $users)
                                                                                    <option value="{{$users->id}}" @if(isset($item) && $users->id == $item->id_user) selected @endif>{{$users->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                {!! $errors->first('id_user', '<p class="help-block">:message</p>') !!}


                                                                            </li>
                                                                        </ul>
                                                                        <ul class="nav flex-column">

                                                                            <li class="nav-item-btn text-right nav-item">

                                                                                    <button  type="submit"class="btn-shadow btn-wide btn-pill btn btn-focus btn-sm">Valider</button>


                                                                            </li>
                                                                        </ul>



                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
