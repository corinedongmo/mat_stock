

<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">Nom<span style="color:red">*</span></label>
    <input class="form-control" name="nom" type="text" id="nom" value="{{ isset($user->nom) ? $user->nom : ''}}"  required>
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('prenom') ? 'has-error' : ''}}">
    <label for="prenom" class="control-label">Prenom<span style="color:red">*</span></label>
    <input class="form-control" name="prenom" type="text" id="prenom" value="{{ isset($user->prenom) ? $user->prenom : ''}}" required>
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('fonction') ? 'has-error' : ''}}">
    <label for="fonction" class="control-label">fonction<span style="color:red">*</span></label>
    <input class="form-control" name="fonction" type="text" id="fonction" value="{{ isset($user->fonction) ? $user->fonction : ''}}" required>
    {!! $errors->first('fonction', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">Email<span style="color:red">*</span></label>
    <input type="email"class="form-control" name="email" type="text" id="email" value="{{ isset($user->email) ? $user->email : ''}}" required>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">Mot de passe<span style="color:red">*</span></label>
    <input  class="form-control" name="password" type="password" id="password" value="{{ isset($user->password) ? $user->password : ''}}" required>

    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
   

</div>






<div class="form-group {{ $errors->has('role') ? 'has-error' : ''}}">
    <label for="role" class="control-label">{{ __('Role ') }}<span style="color:red">*</span></label>


    <select class="form-control" name="role" type="number" id="role" value="{{ isset($user->role) ? $user->role : '' }}" required>
        <option value="">{{ __('Selectionner le role ') }} </option>
        @foreach($role as $role)
        <option value="{{$role->name}}" @if(isset($user) ) selected @endif>{{$role->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('role', '<p class="help-block">:message</p>') !!}

</div>





<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Creer' }}">
</div>
