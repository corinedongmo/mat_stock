
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Dashboard</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

        <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">

        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <link rel="stylesheet" href="{{asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/plugins/jqvmap/jqvmap.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
          {{-- <link rel="stylesheet" href="{{asset('assets/dist/css/autre.css')}}"> --}}

        <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}">

        <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/dist/css/toastr.min.css') }}">

    </head>
@if (Auth::check())
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            @include('layouts.partials.header')
            @include('layouts.partials.sidebar')


            <div class="content-wrapper">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>


                <section class="content">
                    <div class="container-fluid">






                        @yield('content')


                    </div>
                </section>

            </div>

            @include('layouts.partials.footer')
            <aside class="control-sidebar control-sidebar-dark">

            </aside>

        </div>
      
            <script src="{{ asset('assets/delete.js') }}"></script>
              <script src="{{ asset('assets/dist/js/toastr.min.js') }}"></script>
        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

        <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

        <script>
        $.widget.bridge('uibutton', $.ui.button)
        </script>

        <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

        <script src="{{asset('assets/plugins/chart.js/Chart.min.js')}}"></script>

        <script src="{{asset('assets/plugins/sparklines/sparkline.js')}}"></script>

        <script src="{{asset('assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>

        <script src="{{asset('assets/plugins/jquery-knob/jquery.knob.min.js')}}"></script>

        <script src="{{asset('assets/plugins/moment/moment.min.js')}}"></script>
        <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>

        <script src="{{asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>

        <script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>

        <script src="{{asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>

        <script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
        {{-- <script src="{{asset('assets/dist/js/autre.js')}}"></script> --}}
        <script src="{{asset('assets/dist/js/demo.js')}}"></script>

        <script src="{{asset('assets/dist/js/pages/dashboard.js')}}"></script>

        @if (Session::has('message'))
            <script>
                toastr.success("{{ session('message') }}", 'Success', {
                    timeOut: 5000
                });
            </script>
        @endif
        @if (Session::has('error'))
            <script>
                toastr.error("{{ session('error') }}", 'Erreur', {
                    timeOut: 9000
                });
            </script>
        @endif
        @if (Session::has('info'))
            <script>
                toastr.info("{{ session('info') }}", 'Information', {
                    timeOut: 7000
                });
            </script>
        @endif
        @if (Session::has('warning'))
            <script>
                toastr.warning("{{ session('warning') }}", 'Warning', {
                    timeOut: 9000
                });
            </script>
        @endif

    </body>

@else
    {{ route('login') }}
@endif
</html>
