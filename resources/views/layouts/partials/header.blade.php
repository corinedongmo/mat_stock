
<nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        
        
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">15 Notifications</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> 8 friend requests
                <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> 3 new reports
                <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
        <li>

            
            <div class="header-btn-lg pr-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <button type="button" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false" class="p-0 btn btn-link dd-chart-btn">
                                    <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                                        <span class="icon-wrapper-bg bg-success"></span>
                                        <i class="icon text-success ion-ios-analytics"></i>
                                    </span>
                                </button>

                                <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                                                <div class="image">
                                                    <img src="{{url('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                                                </div>
                                                <div class="info">
                                                    <a href="#" class="d-block"> {{ Auth::user()->nom}}</a>
                                                </div>
                                            </div> --}}
                                            <div class="menu-header-content text-left">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">

                                                        <div class="user-panel  pb-3 mb-3 d-flex">
                                                            <div class="widget-heading">
                                                                {{ Auth::user()->nom }}
                                                                {{ Auth::user()->prenom }}
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="user-panel  pb-3 mb-3 d-flex ">
                                       
                                            <ul class="nav flex-column">

                                                <li class="nav-item">
                                                    <a href="#" class="nav-link">Nom 
                                                        <div class="ml-auto badge  badge-alternate">
                                                            {{ Auth::user()->nom }} 

                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#" class="nav-link">Prenom
                                                        <div class="ml-auto badge  badge-alternate">
                                                            {{ Auth::user()->prenom }}

                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="nav-item pull-right ">
                                                    <a href="#" class="nav-link btn btn-alt">Email
                                                        <div class=" btn btn-alt" type="button">
                                                            {{ Auth::user()->email }}
                                                        </div>
                                                    </a>
                                                </li>

                                                <div class="dropdown-divider"></div>

                                                <div class="float-right">
                                                     <form method="POST" action="{{ route('logout') }}">
                                                        @csrf
                                                        <a class="dropdown-item" href="route('logout')"
                                                            onclick="event.preventDefault(); this.closest('form').submit();">
                                                              &nbsp &nbsp &nbsp
                                                            <button
                                                                class="float-right btn-xs btn-primary">{{ __('Deconnecter') }}
                                                            </button>
                                                        </a>
                                                    </form>

                                                </div>





                                            </ul>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                {{ Auth::user()->nom}}
                                  {{ Auth::user()->prenom}}
                            </div>

                        </div>

                    </div>
                
            </div>



        </li>
      
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
    <li class="nav-item">
    <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
    <i class="fas fa-th-large"></i>
    </a>
    </li>
    </ul>
</nav>