<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="#" class="brand-link">
        <img src="{{url('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Stock</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{url('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              {{ Auth::user()->nom}}
            </div>
        </div>

        

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

               <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                          
                        </p>
                    </a>
                   
                </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-sticky-note"></i>
                        <p>
                            Demande
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: none;">
                        <li class="nav-item">
                        @if(Auth::user()->hasRole('superadmin','administrateur'))
                            <a href="{{url('/demande')}}" class="nav-link">
                             &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                 <p>consulter</p>
                            </a>
                            <a href="{{url('/demande_valider')}}" class="nav-link">
                             &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                <p>demande valider</p>
                            </a>
                            <a href="{{url('/demande_refuser')}}" class="nav-link">
                             &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                <p>demande refuser</p>
                            </a>
                        @endif

                        @if(Auth::user()->hasRole('utilisateur'))
                           
                            <a href="{{url('/demande_u')}}"class="nav-link">
                                &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                <p> consulter</p>
                            </a>
                            <a href="{{url('/demande_user_val')}}"class="nav-link">
                                &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                    <p>demande valider</p>
                            </a>
                            <a href="{{url('/demande_user_ref')}}"class="nav-link">
                                    &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                <p> demande refuser</p>
                            </a>          
                                        
                        @endif
                        </li>

                    </ul>
                </li>
               <li class="nav-item">
                   
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-toolbox"></i>
                        <p>
                            Materiel
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: none;">
                        <li class="nav-item">
                            @if(Auth::user()->hasRole('superadmin','administrateur')) 
                                <a href="{{url('/materiel')}}"class="nav-link">
                                 &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            @endif
                            @if(Auth::user()->hasRole('utilisateur')) 
                                <a href="{{url('/materiel_u')}}"class="nav-link">
                                 &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            @endif
                        </li>

                    </ul>
                </li>
                @if(Auth::user()->hasRole('superadmin','administrateur')) 
                    <li class="nav-item">
                        
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>

                            <p>
                                Fournisseur
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="{{url('/fournisseur')}}" class="nav-link">
                                 &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            </li>

                        </ul>   
                       
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-object-group"></i>
                            <p>
                                Categorie materiel
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="{{url('/categorie')}}" class="nav-link">
                                 &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-splotch"></i>
                            <p>
                                Statut
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="{{url('/statut')}}" class="nav-link">
                                 &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif
                
                @if(Auth::user()->hasRole('superadmin')) 
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Utilisateur
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                            <a href="{{url('/user')}}" class="nav-link">
                                &nbsp &nbsp &nbsp
                                <i class="far fa-circle nav-icon"></i>
                                <p>consulter</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class=" fas fa-users-cog"></i>
                            <p>
                                &nbsp &nbsp &nbsp Role
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                            <a href="{{url('/role')}}" class="nav-link">
                                &nbsp &nbsp &nbsp
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>consulter</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif
                
            </ul>
        </nav>

    </div>

</aside>