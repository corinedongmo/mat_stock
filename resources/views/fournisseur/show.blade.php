@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">fournisseur </div>
                    <div class="card-body">
                        <div style="float:right">
                            <a href="{{ url('/fournisseur') }}" title="retour">
                                <button class="btn btn-warning btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> 
                                    Retour
                                </button>
                            </a>
                            <a href="{{ url('/fournisseur/' . $fournisseur->id . '/edit') }}" title="Editer fournisseur">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-pen" aria-hidden="true"></i> Modifier
                                </button>
                            </a>
                        </div>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <th> Nom fournisseur </th>
                                        <td> {{ $fournisseur->nom_fournisseur }} </td>
                                    </tr>
                                    <tr>
                                        <th>localisation </th>
                                        <td> {{ $fournisseur->localisation }} </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
