@extends('layouts.apps')

@section('content')

    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">fournisseur</div>
                    <div class="card-body">
                        <a href="{{ url('/fournisseur/create') }}" class="mb-2 mr-2 btn-hover-shine btn btn-success btn-sm" title="Ajouter fournisseur">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                        </a>
                        &nbsp;&nbsp;

                       <br><br>


                        <div class="main-card mb-3 card">
                            <div class="card-body">


                                <table style="width: 100%;" id="example"
                                    class="table table-hover table-striped table-bordered">

                                    <thead>
                                        <tr>


                                            <th>#</th>
                                            <th> Nom Du fournisseur</th>
                                            <th>localisation</th>

                                            <th>{{__('Actions')}}</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($fournisseur as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>{{ $item->nom_fournisseur }}</td>
                                            <td>{{ $item->localisation }}</td>


                                            <td >

                                                <a href="{{ url('/fournisseur/' . $item->id) }}" title="Voir fournisseur">
                                                    <button class="mb-2 mr-2 btn-hover-shine btn btn-info btn-sm">
                                                        <i class="fa fa-eye" aria-hidden="true">
                                                        </i></button></a>
                                                <a href="{{ url('/fournisseur/' . $item->id . '/edit') }}" title="Editer fournisseur">
                                                    <button class="mb-2 mr-2 btn-hover-shine btn btn-primary btn-sm">
                                                        <i class="fa fa-pen" aria-hidden="true"></i>
                                                    </button>
                                                </a>

                                            
                                                <form method="post" action="{{ url('/fournisseur/delete/' . $item->id) }}"style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        @csrf
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-danger btn-sm delete-confirm" title="supprimer fournisseur"type="submit">
                                                            <i class="fa fa-trash"></i>
                                                           </button>
                                                    </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
