


        <div class="form-group row {{ $errors->has('nom_fournisseur') ? 'has-error' : ''}}">
            <label class="col-sm-12 col-md-2 col-form-label">Nom Du fournisseur</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text"  name="nom_fournisseur"required value="{{isset($fournisseur->nom_fournisseur) ? $fournisseur->nom_fournisseur : ''}}">
            </div>
        </div>
        <div class="form-group row {{ $errors->has('localisation') ? 'has-error' : ''}}">
            <label class="col-sm-12 col-md-2 col-form-label">localisation du fournisseur</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" rows="5" name="localisation" type="textarea" id="localisation" required>{{ isset($fournisseur->localisation) ? $fournisseur->localisation : ''}}</textarea>
                {!! $errors->first('localisation', '<p class="help-block">:message</p>') !!}
            </div>
        </div>





        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? __('Modifier') : __('Creer')  }}">
        </div>

