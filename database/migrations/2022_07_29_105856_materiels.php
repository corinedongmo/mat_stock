<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique();
            $table->string('libelle_materiel')->nullable();
            $table->integer('quantite')->nullable();
            $table->string('images')->nullable();
            $table->integer('id_categorie')->unsigned();
            $table->integer('personnel')->nullable();
            $table->integer('id_statut')->unsigned();
            $table->integer('id_user')->nullable();
            $table->integer('id_user_auth')->unsigned();
            $table->integer('id_fournisseur')->unsigned();
        





            $table->foreign('id_categorie')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_statut')->references('id')->on('statuts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_user_auth')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_fournisseur')->references('id')->on('fournisseurs')->onDelete('cascade')->onUpdate('cascade');
           
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiels');
    }
};
