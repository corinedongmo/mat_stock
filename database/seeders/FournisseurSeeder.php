<?php

namespace Database\Seeders;

use App\Models\Fournisseur;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FournisseurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $etat1 = new Fournisseur();
        $etat1->nom_fournisseur = "compagnie LT";
        $etat1->localisation = "akwa face bicec";
        $etat1->save();

        $etat2 = new Fournisseur();
        $etat2->nom_fournisseur = "accessoire plus";
        $etat2->localisation = "bonapriso derriere lez glacier moderne";
        $etat2->save();

        $etat1 = new Fournisseur();
        $etat1->nom_fournisseur = "agency tech ";
        $etat1->localisation = "rond point deido à 100m de la boulangerie meno ";
        $etat1->save();
    }
}
