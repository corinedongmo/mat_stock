<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {  $this->call(LaratrustSeeder::class);
       // $this->call(RoleSeeder::class);
        $this->call(CategorieSeeder::class);
        $this->call(StatutSeeder::class);
        $this->call(FournisseurSeeder::class);
        $this->call(EtatSeeder::class);
       
        $this->call(UserSeeder::class);
       // $this->call(CategorieSeeder::class);
    }
}
