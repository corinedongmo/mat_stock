<?php

namespace Database\Seeders;

use App\Models\Statut;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statut1 = new Statut();
        $statut1->libelle_statut = "En cours d'utilisation";
        $statut1->description_statut= "il sagit d'un materiel qui est en cours d'usage ";
        $statut1->save();

        $statut2 = new Statut();
        $statut2->libelle_statut = "En panne";
        $statut2->description_statut= "il s'agit du materiel qui se trouve en panne ";
        $statut2->save();

        $statut3 = new Statut();
        $statut3->libelle_statut = "A Depanner";
        $statut3->description_statut= "il s'agit du materiel en attente de reparation";
        $statut3->save();

        $statut4 = new Statut();
        $statut4->libelle_statut = "Reparer";
        $statut4->description_statut= "il s'agit du materiel qui etait en panne et qui a ete repare";
        $statut4->save();

        $statut5 = new Statut();
        $statut5->libelle_statut = "Stocker";
        $statut5->description_statut= "il s'agit du materiel qui n'est pas utilise par une quelconque personne";
        $statut5->save();

        $statut6 = new Statut();
        $statut6->libelle_statut = "besoin";
        $statut6->description_statut= "il s'agit du materiel dont un utilisateur souhaite utiliser ";
        $statut6->save();

    }
}
