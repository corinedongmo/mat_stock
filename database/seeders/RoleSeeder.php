<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = new Role();
        $role1->name = "admin";
        $role1->display_name = "administrateur";
        $role1->description= "personne ayant une visbilité partiel des vues du syteme";
        $role1->save();

        $role2 = new Role();
        $role2->name = "super_admin";
        $role2->display_name = "superadministrateur";
        $role2->description = "personne ayant une visibilité de facon total sur l'application";
        $role2->save();


        $role3 = new Role();
        $role3->name = "user";
        $role1->display_name = "utilisateur";
        $role3->description = "principal acteur du systeme";
        $role3->save();
    }
}
