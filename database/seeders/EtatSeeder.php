<?php

namespace Database\Seeders;

use App\Models\Etat;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EtatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $etat1 = new Etat();
        $etat1->libelle_etat = "En attente";
        $etat1->description_etat = "etat d'une demande d'un utilisateur en attente de traitement";
        $etat1->save();

        $etat2 = new Etat();
        $etat2->libelle_etat = "Valider";
        $etat2->description_etat = "etat d'une demande d'un utilisateur ayant ete validee";
        $etat2->save();

        $etat1 = new Etat();
        $etat1->libelle_etat = "Non Valider";
        $etat1->description_etat = "etat d'une demande d'un utilisateur ayant ete non validee";
        $etat1->save();
    }
}
