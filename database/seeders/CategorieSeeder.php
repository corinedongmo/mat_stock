<?php

namespace Database\Seeders;

use App\Models\Categorie;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorie1 = new Categorie();
        $categorie1->libelle_categorie = "peripherique de stockage";
        $categorie1->description_categorie = "support de stockage des inforamtion";
        $categorie1->save();

        $categorie2 = new Categorie();
        $categorie2->libelle_categorie = "materiel d'assembage";
        $categorie2->description_categorie = "materiel destiné à assembler les éléments";
        $categorie2->save();
    }
}
