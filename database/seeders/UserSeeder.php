<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user2 = new User();
        $user2->nom = "corine";
        $user2->prenom = "dtc";
        $user2->fonction = "stagiaire";
        $user2->email = "corine@gmail.com";
        $user2->password = bcrypt('12345678');
        $user2->role = "superadmin";
        $user2->save();
        $permission=Permission::all();
        $role=Role::all();
        foreach($permission as $item){
            $user2->attachPermission($item);

        }
        foreach($role->where('name','=','superadmin') as $item){
            $user2->attachRole($item);
            
        }
    }
   
}
